<?php



class Doku_Report {
    protected $content = array();
    protected $toc = array();
    protected $language;

    function __construct($language = 'da') {
        $this->language = $language;
        $this->toc = array(
            '#prefix' => '<div class="doku-toc"><ul>',
            '#suffix' => '</ul></div>',
        );

    }

    public function addDoku(Doku $doku, $theme='doku_element', $extra=array()) {
        $this->content[$doku->getModule()]['_title'] = array(
            '#theme' => 'doku_header_module',
            '#module' => $doku->getModule(),
            '#weight' => -50,
        );

        $this->content[$doku->getModule()][$doku->getType()]['_title'] = array(
            '#theme' => 'doku_header_type',
            '#type' => $doku->getType(),
            '#weight' => -50,
        );

        $this->content[$doku->getModule()][$doku->getType()][$doku->getId()]['_title'] = array(
            '#markup' => '<h3>' . $doku->getTitle() . '</h3>',
            '#weight' => -50,
        );

        // the TOC is not themable
        $this->toc[$doku->getModule()]['#prefix'] = '<li>' . $doku->getModule() . '<ul>';
        $this->toc[$doku->getModule()]['#suffix'] = '</ul></li>';

        $this->toc[$doku->getModule()][$doku->getType()]['#prefix'] = '<li>' . $doku->getType() . '<ul>';
        $this->toc[$doku->getModule()][$doku->getType()]['#suffix'] = '</ul></li>';

        $this->toc[$doku->getModule()][$doku->getType()][$doku->getId()] = array(
            '#markup' => sprintf('<li><a href="#%s">%s</a></li>', $doku->getKey(true), $doku->getId())
        );


        $element = array(
            '#theme' => $theme,
            '#title' => $doku->getTitle(),
            '#module' => $doku->getModule(),
            '#type' => $doku->getType(),
            '#id' => $doku->getId(),
            '#key' => $doku->getKey(),
            '#links' => $doku->getLinks(),
        );

        foreach ($extra as $k => $v)
            $element['#' . $k] = $v;

        $this->content[$doku->getModule()][$doku->getType()][$doku->getId()][] = $element;


    }

    public function getAll () {
        return array(
            '#prefix' => '<div class="doku-report">',
            'toc' => $this->getTOC(),
            'content' => $this->getContent(),
            '#suffix' => '</div>',
        );
    }

    public function getContent() {
        return $this->content;
    }



    public function getTOC() {
        return $this->toc;

    }
e


}

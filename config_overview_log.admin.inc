<?php

  function config_overview_log_filter_form($form) {
return;
    $modules = db_select('doku_log')
      ->fields('doku_log', array('module'))
      ->orderBy('module')
      ->execute()
      ->fetchAllKeyed(0, 0);

    $users = db_select('doku_log')
      ->fields('doku_log', array('uid'));
    $users->leftJoin('users', 'u', 'doku_log.uid = u.uid');
    $users->addField('u', 'name');
    $users=$users->execute()
      ->fetchAllKeyed(0, 1);
    
    $modules[0] = t('All');
    $users[0] = t('All');
    
    return array(
      'filters' => array(
        '#type' => 'fieldset',
        '#title' => t('Filter log messages'),
        '#collapsible' => TRUE,
        '#collapsed' => empty($_SESSION['config_overview_log_overview_filter']),     
  
        'module' => array(
          '#title' => t('Module'),
          '#type' => 'select',
          '#multiple' => TRUE,
          '#size' => 8,
          '#options' => $modules,        
          '#default_value' => 0,
        ),
        'user' => array(
          '#title' => t('User'),
          '#type' => 'select',
          '#multiple' => TRUE,
          '#size' => 8,
          '#options' => $users,        
        ),
        'submit' => array(
          '#type' => 'submit',
          '#value' => t('Search'),
        ),
      ),
    );
  }
  
  function config_overview_log_filter_form_submit($form, &$form_state) {
    $op = $form_state['values']['op'];
    $filters = dblog_filters();
    switch ($op) {
      case t('Filter'):
        foreach ($filters as $name => $filter) {
          if (isset($form_state['values'][$name])) {
            $_SESSION['dblog_overview_filter'][$name] = $form_state['values'][$name];
          }
        }
        break;
      case t('Reset'):
        $_SESSION['dblog_overview_filter'] = array();
        break;
    }
    return 'admin/reports/dblog';
  }

  function config_overview_log_overview() {
    $header = array(
      array(
        'data' => t('Date'),          
        'field' => 'doku_log.timestamp',
        'sort' => 'desc',
      ),
      array(
        'data' => t('Message'),          
        'field' => 'doku_log.message',
      ),
      array(
        'data' => t('User'),          
        'field' => 'u.name',
      ),
      array(
        'data' => t('Operations'),          
      ),
    );  

    
    $build['form'] = drupal_get_form('config_overview_log_filter_form');
    
    $logs = db_select('doku_log')->extend('PagerDefault')->extend('TableSort')
      ->fields('doku_log');
    $logs->leftJoin('users', 'u', 'doku_log.uid = u.uid');
    $logs->addField('u', 'name');
    $logs=$logs->limit(50)
      ->orderByHeader($header);
    $logs=$logs->execute();
      
    $rows = array();
    while ($log = $logs->fetch()) {

      $doku = new Doku(        
        $log->module,
        $log->type,
        $log->eid
      );
      
      $vars = unserialize($log->variables);
      $title = $doku->getTitle();
      $msg = t($log->message, $vars);
      
      if ($title != $vars['@title'])
        $msg .= ' (' . t('Title now : %title', array('%title' => $title)) . ')';
        
      $rows[] = array(
        format_date($log->timestamp, 'short'),
        l(substr($msg, 0, 100) . (strlen($msg)>100?' ...':''), 'admin/reports/doku/log/' . $log->lid),
        theme('username', array('account' => $log)),
        l(t('Edit'), trim($log->location, '/')),
      );
          
        
    }  
      
    
    $build['table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => array('id' => 'admin-doku_log'),
      '#empty' => t('No log messages available.'),
    );

    $build['pager'] = array('#theme' => 'pager');

    return $build;
  }
  
  function config_overview_log_event($lid) {
    $log = db_select('doku_log')
      ->fields('doku_log')
      ->condition('lid', $lid);
    $log->leftJoin('users', 'u', 'doku_log.uid = u.uid');
    $log->addField('u', 'name');
    $log=$log->execute()->fetchObject();
      

    $doku = new Doku(        
      $log->module,
      $log->type,
      $log->eid
    );
    
      
      if ($log) {
        $vars = unserialize($log->variables);
            
        $msg = t($log->message, $vars);
        $rows = array(
          array(
            array('data' => t('Date'), 'header' => TRUE),
            format_date($log->timestamp, 'long'),
          ),
          array(
            array('data' => t('Element'), 'header' => TRUE),
            $doku->getFullTitle(),
          ),
          array(
            array('data' => t('User'), 'header' => TRUE),
            theme('username', array('account' => $log)),
          ),
          array(
            array('data' => t('Location'), 'header' => TRUE),
            l($log->location, $log->location),
          ),
          array(
            array('data' => t('Message'), 'header' => TRUE),
            theme('dblog_message', array('event' => $log)),
          ),
        );
        $build['log_table'] = array(
          '#theme' => 'table',
          '#rows' => $rows,
          '#attributes' => array('class' => array()),
        );

        $fields = unserialize($log->fields);      
        $fr=array();
        if (is_array($fields))
          foreach ($fields as $field) {
            $fr[] = array(
              array('data' => $field['l'], 'header' => TRUE),
              $field['k'],
              $field['v'],
            );
          }
        $build['field_table'] = array(
          '#theme' => 'table',
          '#rows' => $fr,
          '#attributes' => array('class' => array()),
        );

        return $build;
          
        
    }  
    
  }
  